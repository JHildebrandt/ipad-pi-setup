#!/bin/bash

cd /sys/kernel/config/usb_gadget/
mkdir -p display-pi
cd display-pi
echo 0x1d6b > idVendor # Linux Foundation
echo 0x0104 > idProduct # Multifunction Composite Gadget
echo 0x0100 > bcdDevice # v1.0.0
echo 0x0200 > bcdUSB # USB2
#echo 0xEF > bDeviceClass
#echo 0x02 > bDeviceSubClass
#echo 0x01 > bDeviceProtocol
mkdir -p strings/0x409
echo "6B296AB5-9C92-4C6E" > strings/0x409/serialnumber
echo "Justus Hildebrandt" > strings/0x409/manufacturer
echo "Pi USB-C Network" > strings/0x409/product
mkdir -p configs/c.1/strings/0x409
echo "Config 1: ECM network" > configs/c.1/strings/0x409/configuration
echo 250 > configs/c.1/MaxPower
# Add functions here
# see gadget configurations below
# End functions

mkdir -p functions/ecm.usb0
HOST="94:6D:6E:C0:4D:EF" # "HostPC"
SELF="1A:79:A8:E7:20:0C" # "BadUSB"
echo $HOST > functions/ecm.usb0/host_addr
echo $SELF > functions/ecm.usb0/dev_addr
ln -s functions/ecm.usb0 configs/c.1/

mkdir -p functions/acm.usb0
ln -s functions/acm.usb0 configs/c.1/

mkdir -p functions/mass_storage.usb0
echo 0 > functions/mass_storage.usb0/stall
echo 0 > functions/mass_storage.usb0/lun.0/cdrom
echo 1 > functions/mass_storage.usb0/lun.0/ro
echo 0 > functions/mass_storage.usb0/lun.0/nofua
echo /opt/disk.img > functions/mass_storage.usb0/lun.0/file
ln -s functions/mass_storage.usb0 configs/c.1/


udevadm settle -t 5 || :
ls /sys/class/udc > UDC

ifup usb0

