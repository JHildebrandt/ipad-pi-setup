#!/bin/bash

# Install dnsmasq DNS server
echo "Installing dnsmasq..."
sudo apt-get update
sudo apt-get install -y dnsmasq
sudo apt-get clean

# Modify configs
# Set colors
OK='\033[0;32m'
CHANGED='\033[1;33m'
NC='\033[0m'
echo "Modifying configs..."
# Modify /boot/config.txt
if grep -Fxq "dtoverlay=dwc2" /boot/config.txt; then
  echo -e "${OK}[ OK ]${NC} /boot/config.txt"
else
  sudo sed -i 's/dtoverlay=.*/dtoverlay=dwc2/g' /boot/config.txt
  echo -e "${CHANGED}[ Changed ]${NC} /boot/config.txt"
fi
# Modify /boot/cmdline.txt
if grep -Fq "modules-load=dwc2" /boot/cmdline.txt; then
  echo -e "${OK}[ OK ]${NC} /boot/cmdline.txt"
else
  sudo sed -i 's/$/ modules-load=dwc2/' /boot/cmdline.txt
  echo -e "${CHANGED}[ Changed ]${NC} /boot/cmdline.txt"
fi
# Modify /etc/modules
if grep -Fxq "libcomposite" /etc/modules; then
  echo -e "${OK}[ OK ]${NC} /etc/modules"
else
  echo "libcomposite" | sudo tee -a /etc/modules > /dev/null
  echo -e "${CHANGED}[ Changed ]${NC} /etc/modules"
fi
# Modify /etc/dhcpcd.conf
if grep -Fxq "denyinterfaces usb0" /etc/dhcpcd.conf; then
  echo -e "${OK}[ OK ]${NC} /etc/dhcpcd.conf"
else
  echo "denyinterfaces usb0" >> /etc/dhcpcd.conf
  echo -e "${CHANGED}[ Changed ]${NC} /etc/dhcpcd.conf"
fi

# Copy configs, script and service
echo "Copying stuff..."
sudo cp usb0-dnsmasq /etc/dnsmasq.d/usb0
sudo cp usb0-interface /etc/network/interfaces.d/usb0
sudo cp usb-gadget.sh /usr/local/sbin/usb-gadget.sh
sudo cp usbgadget.service /lib/systemd/system/usbgadget.service

# Make script executable and enable service
echo "Enabling usbgadget service"
sudo systemctl enable usbgadget.service
echo "Makeing usb-gadget script executable..."
sudo chmod +x /usr/local/sbin/usb-gadget.sh
