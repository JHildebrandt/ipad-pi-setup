#!/bin/bash

sudo apt-get update
sudo apt-get install \
  autoconf \
  automake \
  autopoint \
  autotools-dev \
  binfmt-support \
  bison debhelper \
  dh-autoreconf \
  dh-strip-nondeterminism \
  diffstat \
  dwz \
  flex \
  intltool-debian \
  libarchive-zip-perl \
  libbison-dev \
  libclang-7-dev \
  libclang-common-7-dev
  libclang1-7 \
  libclc-dev \
  libdebhelper-perl \
  libdrm-dev \
  libdrm-etnaviv1 \
  libdrm-exynos1 \
  libdrm-freedreno1 \
  libdrm-omap1 \
  libdrm-tegra0 \
  libegl-dev \
  libelf-dev \
  libffi-dev \
  libfile-stripnondeterminism-perl \
  libgc1c2 \
  libgl-dev \
  libgles-dev \
  libglvnd-core-dev \
  libglvnd-dev \
  libglx-dev \
  libllvm7 \
  libobjc-8-dev \
  libobjc4 \
  libpthread-stubs0-dev \
  libsensors4-dev \
  libset-scalar-perl \
  libsigsegv2 \
  libtool \
  libva-dev \
  libva-glx2 \
  libva-wayland2 \
  libvdpau-dev \
  libvulkan-dev \
  libwayland-bin \
  libwayland-dev \
  libwayland-egl-backend-dev \
  libx11-dev \
  libx11-xcb-dev \
  libxau-dev \
  libxcb-dri2-0-dev \
  libxcb-dri3-dev \
  libxcb-glx0-dev \
  libxcb-present-dev \
  libxcb-randr0-dev \
  libxcb-render0-dev \
  libxcb-shape0-dev \
  libxcb-sync-dev \
  libxcb-xfixes0-dev \
  libxcb1-dev \
  libxdamage-dev \
  libxdmcp-dev \
  libxext-dev \
  libxfixes-dev \
  libxrandr-dev \
  libxrender-dev \
  libxshmfence-dev \
  libxxf86vm-dev \
  llvm-7 \
  llvm-7-dev \
  llvm-7-runtime \
  m4 \
  meson \
  ninja-build \
  po-debconf \
  python3-mako \
  quilt \
  wayland-protocols \
  x11proto-core-dev \
  x11proto-damage-dev \
  x11proto-dev \
  x11proto-fixes-dev \
  x11proto-randr-dev \
  x11proto-xext-dev \
  x11proto-xf86vidmode-dev \
  xorg-sgml-doctools \
  xtrans-dev
