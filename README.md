# iPad Pi Setup

Setup scripts for a Raspberry Pi to be usable with an iPad, or similar device, trough a USB-C connection, as a local dev environment.

## USB network setup

The script is based on [Ben Hardill's work](https://www.hardill.me.uk/wordpress/2019/11/02/pi4-usb-c-gadget/)

Setup usb networking:

```bash
cd ipad-pi-setup/usb-network
./setup.sh
```

## Install packages

```bash
cd ipad-pi-setup/packages
./install_packages.sh
```
