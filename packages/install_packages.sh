#!/bin/bash

CODE_VERSION="3.4.1"
FISH_VERSION="3.1.2"
DOCKER_VERSION="18.09.9"

GIT_USER="justus"
GIT_EMAIL="justus@xmatetech.com"

install_apt_packages() {
  sudo apt-get update
  sudo apt-get install -y \
    git \
    vim \
    tmux \
    software-properties-common \
    build-essential \
    python3-pip \
    jq \
    tldr \
    rclone \
    cmake \
    ncurses-dev \
    libncurses5-dev \
    libpcre2-dev \
    gettext
    snapd
}

install_pip_packages() {
  pip3 install \
    ansible \
    yq
}

install_kube_packages() {
  sudo snap install kubectl --classic
  sudo snap install helm --classic
}

install_docker() {
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
  sudo add-apt-repository \
    "deb [arch=arm64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"
  sudo apt-get update
  sudo apt-get install -y \
    docker-ce=5:${DOCKER_VERSION}~3-0~debian-buster
  sudo usermod -aG docker $(whoami)
}

install_k3s() {
  # Set colors
  OK='\033[0;32m'
  CHANGED='\033[1;33m'
  NC='\033[0m'
  # Enable cgroup support
  if grep -Fq "cgroup_memory=1 cgroup_enable=memory" /boot/cmdline.txt; then
    echo -e "${OK}[ OK ]${NC} cgroup support already enabled."
  else
    sudo sed -i 's/$/ cgroup_memory=1 cgroup_enable=memory/' /boot/cmdline.txt
    echo -e "${CHANGED}[ Changed ]${NC} enabled cgroup support."
  fi
  # Install k3s
  ## FIXME!
  # curl https://github.com/rancher/k3s/releases/download/v1.18.4%2Bk3s1/k3s-arm64 \
  #   -L -o k3s
  # sudo mv k3s /usr/local/bin/
  # sudo chmod +x /usr/local/bin/k3s

  # Until manual install works :(
  curl -sfL https://get.k3s.io | sh -
}

install_code_server() {
  curl "https://github.com/cdr/code-server/releases/download/v${CODE_VERSION}/code-server_${CODE_VERSION}_arm64.deb" \
    -L -o code-server.deb
  sudo dpkg -i code-server.deb
  rm -rf code-server.deb

  while read -r plugin; do
    code-server --install-extension "${plugin}"
  done < plugins

  cp settings.json ~/.local/share/code-server/User/settings.json
}

install_fish() {
  curl https://github.com/fish-shell/fish-shell/releases/download/$FISH_VERSION/fish-$FISH_VERSION.tar.gz \
  -L -O fish-$FISH_VERSION.tar.gz 
  tar -xzvf fish-$FISH_VERSION.tar.gz
  cd fish-$FISH_VERSION

  cmake .
  make
  sudo make install

  echo /usr/local/bin/fish | sudo tee -a /etc/shells
  chsh -s /usr/local/bin/fish

  cd ..
  rm -rf fish-$FISH_VERSION.tar.gz
}

install_fish_completions() {
  mkdir -p ~/.config/fish/completions
  curl https://raw.githubusercontent.com/evanlucas/fish-kubectl-completions/master/completions/kubectl.fish \
      -L -o ~/.config/fish/completions/kubectl.fish

}

style_fish() {
  curl -L https://get.oh-my.fish | fish
  exit
  omf install bobthefish
}

setup_git() {
  git config --global user.email "${GIT_EMAIL}}"
  git config --global user.name "${GIT_USER}}"
}

main() {
  install_apt_packages
  install_pip_packages
  install_kube_packages
  install_docker
  install_k3s
  install_code_server
  install_fish
  install_fish_completions
  style_fish
  setup_git
}

main
